from userbot.utils import register

@register(outgoing=True, pattern="^.group$")

async def join(e):

    if not e.text[0].isalpha() and e.text[0] not in ("/", "#", "@", "!"):

        await e.edit("This is my community.\n\n[Channel✅](http://t.me/PremiumArenaOfficial)\n\n[Chat Group✅](https://t.me/TECHNOLOGICALWORLD)\n\n[UserBot Tutorial - FRIDAY](https://t.me/FRIDAYSUPPORTOFFICIAL)\n\n[FRIDAY Chat](https://t.me/FRIDAYSUPPORTCHAT)\n\n[Github](https://github.com/midhunkm1294-bit/FRIDAY)")
